REST based API for Falabella's technical test:

REQUEST

GET -> http://localhost:8200/api/falabella/products


 
POST -> http://localhost:8200/api/falabella/products

{
    "sku": 1000002,
    "brand": "New Brand",
    "name": "New Product3",
    "size": "Small",
    "price": 250,
    "image": "test",
    "optionalImages": [
        "A",
        "B",
        "C"
    ]
}