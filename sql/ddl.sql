DROP TABLE public.brand CASCADE;
DROP TABLE public.product CASCADE;
DROP TABLE public.optionalimage CASCADE;
DROP TABLE public.optionalimagedetail CASCADE;

CREATE TABLE public.brand (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	name varchar(128) NOT NULL,
	createdat timestamp NOT NULL DEFAULT now(),
	updatedat timestamp NULL,
	active bool NOT NULL,
	CONSTRAINT brand_name_unique UNIQUE (name),
	CONSTRAINT product_pkey PRIMARY KEY (id)
);
CREATE INDEX brand_name_index ON public.brand USING btree (name);

CREATE TABLE public.product (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	idbrand int4 NOT NULL,
	name varchar(128) NOT NULL,
	"size" varchar(128) NULL,
	price int8 NOT NULL,
	image varchar NOT NULL,
	createdat timestamp NOT NULL DEFAULT now(),
	updatedat timestamp NULL,
	active bool NOT NULL,
	sku int8 NOT NULL,
	CONSTRAINT product_pkey1 PRIMARY KEY (id),
	CONSTRAINT product_sku_unique UNIQUE (sku),
	CONSTRAINT product_idbrand_fk FOREIGN KEY (idbrand) REFERENCES brand(id)
);
CREATE INDEX fki_product_idbrand_fk ON public.product USING btree (idbrand);
CREATE INDEX product_name_index ON public.product USING btree (name);
CREATE INDEX product_sku_index ON public.product USING btree (sku);

CREATE TABLE public.optionalimage (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	idproduct int8 NOT NULL,
	createdat timestamp NOT NULL DEFAULT now(),
	updatedat timestamp NULL,
	active bool NOT NULL,
	CONSTRAINT optionalimage_idproduct_unique UNIQUE (idproduct),
	CONSTRAINT optionalimage_pkey PRIMARY KEY (id),
	CONSTRAINT optionalimage_idproduct_fk FOREIGN KEY (idproduct) REFERENCES product(id)
);
CREATE INDEX optionalimage_idproduct_index ON public.optionalimage USING btree (idproduct);

CREATE TABLE public.optionalimagedetail (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	idoptionalimage int8 NOT NULL,
	url varchar(128) NOT NULL,
	createdat timestamp NOT NULL DEFAULT now(),
	updatedat timestamp NULL,
	active bool NOT NULL,
	CONSTRAINT optionalimagedetail_pkey PRIMARY KEY (id),
	CONSTRAINT optionalimagendetail_idoptionalimage_fk FOREIGN KEY (idoptionalimage) REFERENCES optionalimage(id)
);
CREATE INDEX fki_optionalimagendetail_idoptionalimage_fk ON public.optionalimagedetail USING btree (idoptionalimage);
CREATE INDEX optionalimagedetail_idoptionalimage_index ON public.optionalimagedetail USING btree (idoptionalimage);