package cl.falabella.product.repositories;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;

import cl.falabella.product.domain.entity.OptionalImage;
import cl.falabella.product.infrastructure.repository.OptionalImageRepository;

@ExtendWith(MockitoExtension.class)
class OptionalImageRepositoryTests {
	
	@Mock
	private JdbcTemplate jdbcTemplate;
	
	@InjectMocks
	private OptionalImageRepository repository;
	
	private OptionalImage optionalImage;
	
	@BeforeEach
	void setupObject() {
		this.optionalImage = new OptionalImage();
	}
	
	@Test
	void shouldValidateGet() {
		when(jdbcTemplate.query(Mockito.anyString(), ArgumentMatchers.<BeanPropertyRowMapper<OptionalImage>>any(), ArgumentMatchers.<Object>any())).thenReturn(null);
		Assertions.assertNull(repository.get("test"));
	}
	
	@Test
	void shouldValidateSave() {
		when(jdbcTemplate.update(Mockito.any(PreparedStatementCreator.class), Mockito.any(KeyHolder.class))).thenReturn(1);
		Assertions.assertThrows(NullPointerException.class, () -> repository.save(optionalImage));
	}

}
