package cl.falabella.product.repositories;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import cl.falabella.product.domain.entity.OptionalImage;
import cl.falabella.product.domain.entity.OptionalImageDetail;
import cl.falabella.product.infrastructure.repository.OptionalImageDetailRepository;

@ExtendWith(MockitoExtension.class)
class OptionalImageDetailRepositoryTests {
	
	@Mock
	private JdbcTemplate jdbcTemplate;
	
	@InjectMocks
	private OptionalImageDetailRepository repository;
	
	private List<OptionalImageDetail> imageList;
	private OptionalImageDetail image;
	
	@BeforeEach
	void setupObject() {
		this.imageList = new ArrayList<>();
		this.image = new OptionalImageDetail();
	}
	
	@Test
	void shouldValidateGet() {
		when(jdbcTemplate.query(Mockito.anyString(), ArgumentMatchers.<BeanPropertyRowMapper<OptionalImage>>any(), ArgumentMatchers.<Object>any())).thenReturn(Collections.emptyList());
		Assertions.assertEquals(Collections.emptyList(), repository.get(0L));
	}
	
	@Test
	void shouldValidateSaveAll() {
		int[] batch = new int[2];
		when(jdbcTemplate.batchUpdate(Mockito.anyString(), Mockito.any(BatchPreparedStatementSetter.class))).thenReturn(batch);
		
		image.setUrl("Test");
		imageList.add(image);
		Assertions.assertDoesNotThrow(() -> { repository.saveAll(0L, imageList); });
	}

}
