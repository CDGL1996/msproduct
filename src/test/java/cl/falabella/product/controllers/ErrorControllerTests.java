package cl.falabella.product.controllers;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;

import cl.falabella.product.infrastructure.controller.ErrorController;

@ExtendWith(MockitoExtension.class)
class ErrorControllerTests {
	
	@Mock
	private BindingResult bindingResult;
	
	@Mock
	private HttpServletRequest request;
	
	@InjectMocks
	private ErrorController controller;
	
	@Test
	void shouldValidateMethodArgumentNotValidExceptionHandler() {
		List<FieldError> errorList = new ArrayList<>();
		FieldError error = new FieldError("test", "test", "test");
		errorList.add(error);
		
		when(bindingResult.getFieldErrors()).thenReturn(errorList);
		MethodArgumentNotValidException exception = new MethodArgumentNotValidException(null, bindingResult);
		
		Assertions.assertEquals(400, controller.methodArgumentNotValidExceptionHandler(request, exception).getStatusCodeValue());
	}
	
	@Test
	void shouldValidateMissingServletRequestExceptionHandler() {
		Assertions.assertEquals(400, controller.exceptionApplicationHandler(request, new MissingServletRequestParameterException("test", "test")).getStatusCodeValue());
	}
	
	@Test
	void shouldValidateExceptionHandler() {
		Assertions.assertEquals(500, controller.exceptionHandler(request, new Exception("test")).getStatusCodeValue());
	}
	
	@Test
	void shouldValidateApplicationExceptionHandler() {
		Assertions.assertEquals(500, controller.exceptionHandler(request, new Exception("test")).getStatusCodeValue());
	}

}
