package cl.falabella.product.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cl.falabella.product.application.dto.in.ProductDTO;
import cl.falabella.product.application.port.interactor.IProductService;
import cl.falabella.product.infrastructure.controller.ProductController;

@ExtendWith(MockitoExtension.class)
class ProductControllerTests {
	
	@Mock
	private IProductService service;
	
	@InjectMocks
	private ProductController controller;
	
	private MockMvc mockMvc;
	
	private static final String URL_PREFIX = "/products";
	
	private ProductDTO dto;
	
	@BeforeEach
	void setupObject() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
		this.dto = new ProductDTO();
	}
	
	@Test
	void shouldValidateGet() throws Exception {
		MvcResult result = this.mockMvc.perform(get(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(200, statusCode);
	}
	
	@Test
	void shouldValidateSave() throws JsonProcessingException, Exception {
		dto.setBrand("test");
		dto.setImage("test");
		dto.setName("test");
		dto.setPrice(23L);
		dto.setSize("test");
		dto.setSku(1000000L);
		
		MvcResult result = this.mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(dto)))
                .andDo(print())
                .andReturn();

        int statusCode = result.getResponse().getStatus();
        Assertions.assertEquals(201, statusCode);
	}

}
