package cl.falabella.product.services;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cl.falabella.product.application.ProductService;
import cl.falabella.product.application.dto.in.ProductDTO;
import cl.falabella.product.application.port.interactor.IBrandService;
import cl.falabella.product.application.port.interactor.IOptionalImageDetailService;
import cl.falabella.product.application.port.interactor.IOptionalImageService;
import cl.falabella.product.application.port.output.IProductRepository;
import cl.falabella.product.domain.entity.Brand;
import cl.falabella.product.domain.entity.OptionalImage;
import cl.falabella.product.domain.entity.OptionalImageDetail;
import cl.falabella.product.domain.entity.Product;

@ExtendWith(MockitoExtension.class)
class ProductServiceTests {
	
	@Mock
	private IBrandService brandService;
	
	@Mock
	private IOptionalImageService optionalImageService;
	
	@Mock
	private IOptionalImageDetailService optionalImageDetailService;
	
	@Mock
	private IProductRepository repository;
	
	@InjectMocks
	private ProductService service;
	
	private ProductDTO dto;
	private List<String> optionalImages;
	
	private Brand brand;
	private Product product;
	private OptionalImage optionalImage;
	private List<OptionalImageDetail> imageList;
	private OptionalImageDetail imageDetail;
	
	@BeforeEach
	void setupObject() {
		this.dto = new ProductDTO();
		this.optionalImages = new ArrayList<>();
		
		this.brand = new Brand();
		this.product = new Product();
		this.optionalImage = new OptionalImage();
		this.imageList = new ArrayList<>();
		this.imageDetail = new OptionalImageDetail();
	}
	
	@Test
	void shouldValidateGet() {
		product.setId(1L);
		product.setIdBrand(1);
		product.setName("test");
		when(repository.get(Mockito.anyLong())).thenReturn(product);
		
		brand.setId(1);
		brand.setName("test");
		when(brandService.get(Mockito.anyInt())).thenReturn(brand);
		
		optionalImage.setId(1L);
		optionalImage.setIdProduct(1L);
		when(optionalImageService.get(Mockito.anyString())).thenReturn(optionalImage);
		
		imageDetail.setUrl("test");
		imageList.add(imageDetail);
		when(optionalImageDetailService.get(Mockito.anyLong())).thenReturn(imageList);
		
		Assertions.assertNotNull(service.get(1L));
	}
	
	@Test
	void shouldValidateSave() {
		brand.setId(1);
		when(brandService.save(Mockito.any(Brand.class))).thenReturn(brand);
		
		product.setId(1L);
		product.setName("test");
		when(repository.save(Mockito.any(Product.class))).thenReturn(product);
		
		optionalImage.setId(1L);
		when(optionalImageService.save(Mockito.anyString(), Mockito.any(OptionalImage.class), ArgumentMatchers.<List<String>>any())).thenReturn(optionalImage);
		
		imageList.add(imageDetail);
		when(optionalImageDetailService.saveAll(Mockito.anyLong(), ArgumentMatchers.<List<OptionalImageDetail>>any())).thenReturn(imageList);
	
		dto.setBrand("test");
		dto.setImage("test");
		dto.setName("test");
		dto.setPrice(25L);
		dto.setSize("test");
		dto.setSku(1234212512525L);

		optionalImages.add("test");
		dto.setOptionalImages(optionalImages);
		
		Assertions.assertNotNull(service.save(dto));
	}

}
