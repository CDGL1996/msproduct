package cl.falabella.product.services;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cl.falabella.product.application.OptionalImageService;
import cl.falabella.product.application.port.output.IOptionalImageRepository;
import cl.falabella.product.domain.entity.OptionalImage;

@ExtendWith(MockitoExtension.class)
class OptionalImageServiceTests {
	
	@Mock
	private IOptionalImageRepository repository;
	
	@InjectMocks
	private OptionalImageService service;
	
	private OptionalImage optionalImage;
	private List<String> imageList;
	
	@BeforeEach
	void setupObject() {
		this.optionalImage = new OptionalImage();
		this.imageList = new ArrayList<>();
	}
	
	@Test
	void shouldValidateGet() {
		when(repository.get(Mockito.anyString())).thenReturn(optionalImage);
		Assertions.assertEquals(optionalImage, service.get("test"));
	}
	
	@Test
	void shouldValidateSave() {
		when(repository.get(Mockito.anyString())).thenReturn(null);
		when(repository.save(Mockito.any(OptionalImage.class))).thenReturn(optionalImage);
		
		imageList.add("Test");
		Assertions.assertEquals(optionalImage, service.save("test", optionalImage, imageList));
		
		when(repository.get(Mockito.anyString())).thenReturn(optionalImage);
		Assertions.assertEquals(optionalImage, service.save("test", optionalImage, imageList));
	}

}
