package cl.falabella.product.services;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cl.falabella.product.application.OptionalImageDetailService;
import cl.falabella.product.application.port.output.IOptionalImageDetailRepository;
import cl.falabella.product.domain.entity.OptionalImageDetail;

@ExtendWith(MockitoExtension.class)
class OptionalImageDetailServiceTests {
	
	@Mock
	private IOptionalImageDetailRepository repository;
	
	@InjectMocks
	private OptionalImageDetailService service;
	
	private List<OptionalImageDetail> imageList;
	private OptionalImageDetail image;
	
	@BeforeEach
	void setupObject() {
		this.imageList = new ArrayList<>();
		this.image = new OptionalImageDetail();
	}
	
	@Test
	void shouldValidateGet() {
		when(repository.get(Mockito.anyLong())).thenReturn(Collections.emptyList());
		Assertions.assertEquals(Collections.emptyList(), service.get(0L));
	}
	
	@Test
	void shouldValidateSaveAll() {
		when(repository.get(Mockito.anyLong())).thenReturn(Collections.emptyList());
		when(repository.saveAll(Mockito.anyLong(), ArgumentMatchers.<List<OptionalImageDetail>>any())).thenReturn(imageList);
		Assertions.assertEquals(imageList, service.saveAll(0L, imageList));
		
		image.setUrl("test");
		imageList.add(image);
		when(repository.get(Mockito.anyLong())).thenReturn(imageList);
		Assertions.assertEquals(imageList, service.saveAll(0L, imageList));
	}

}
