package cl.falabella.product.services;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cl.falabella.product.application.BrandService;
import cl.falabella.product.application.port.output.IBrandRepository;
import cl.falabella.product.domain.entity.Brand;

@ExtendWith(MockitoExtension.class)
class BrandServiceTests {
	
	@Mock
	private IBrandRepository repository;
	
	@InjectMocks
	private BrandService service;
	
	private Brand brand;
	
	@BeforeEach
	void setupObject() {
		this.brand = new Brand();
	}
	
	@Test
	void shouldValidateGet() {
		when(repository.get(Mockito.anyString())).thenReturn(null);
		Assertions.assertNull(service.get("test"));
	}
	
	@Test
	void shouldValidateSave() {
		when(repository.get(Mockito.anyString())).thenReturn(null);
		when(repository.save(Mockito.any(Brand.class))).thenReturn(brand);
		
		brand.setName("test");
		Assertions.assertEquals(brand, service.save(brand));
		
		when(repository.get(Mockito.anyString())).thenReturn(brand);
		Assertions.assertEquals(brand, service.save(brand));
	}

}
