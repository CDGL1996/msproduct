package cl.falabella.product.domain.entity;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OptionalImage {
	
	private Long id;
	private Long idProduct;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private boolean active;

}
