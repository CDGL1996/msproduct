package cl.falabella.product.domain.entity;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {
	
	private Long id;
	private Long sku;
	private Integer idBrand;
	private String name;
	private String size;
	private Long price;
	private String image;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private boolean active;

}
