package cl.falabella.product.domain.entity;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OptionalImageDetail {
	
	private Long id;
	private Long idOptionalImage;
	private String url;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private boolean active;

}
