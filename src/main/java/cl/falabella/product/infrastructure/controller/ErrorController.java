package cl.falabella.product.infrastructure.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import cl.falabella.product.application.ApplicationException;
import cl.falabella.product.application.ErrorDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class ErrorController {

	private ErrorDTO error;
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDTO> exceptionHandler(HttpServletRequest request, Exception e) {
		error = new ErrorDTO();
		error.setCode(10);
		error.setMessage(e.getMessage());
		error.setUrl(request.getRequestURI());
		
		log.error("Error", e);
		
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResponseEntity<ErrorDTO> exceptionApplicationHandler(HttpServletRequest request,
			MissingServletRequestParameterException e) {
		error = new ErrorDTO();
		error.setCode(11);
		error.setMessage(e.getMessage());
		error.setUrl(request.getRequestURI());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<List<ErrorDTO>> methodArgumentNotValidExceptionHandler(HttpServletRequest request, MethodArgumentNotValidException e) {
		BindingResult result = e.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();
		StringBuilder errorMessage = new StringBuilder();
		fieldErrors.forEach(f -> errorMessage.append(f.getField() + " " + f.getDefaultMessage() + " "));
		
		List<ErrorDTO> validationErrors = new ArrayList<>();
		
		for(FieldError fieldError : fieldErrors) {
			error = new ErrorDTO();
			error.setCode(12);
			error.setMessage(fieldError.getDefaultMessage());
			error.setUrl(request.getRequestURI());
			validationErrors.add(error);
		}
		
		return new ResponseEntity<>(validationErrors, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ApplicationException.class)
	public ResponseEntity<ErrorDTO> applicationExceptionHandler(HttpServletRequest request, ApplicationException e) {
		error = new ErrorDTO();
		error.setCode(13);
		error.setMessage(e.getMessage());
		error.setUrl(request.getRequestURI());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
