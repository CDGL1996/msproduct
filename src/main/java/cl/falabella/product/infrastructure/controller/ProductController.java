package cl.falabella.product.infrastructure.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.falabella.product.application.dto.in.ProductDTO;
import cl.falabella.product.application.port.input.IProductController;
import cl.falabella.product.application.port.interactor.IProductService;

@RequestMapping("/products")
@RestController
public class ProductController implements IProductController {
	
	private final IProductService service;
	
	@Autowired
	public ProductController(IProductService service) {
		this.service = service;
	}

	@GetMapping()
	@Override
	public ResponseEntity<List<ProductDTO>> get() {
		return new ResponseEntity<>(service.get(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	@Override
	public ResponseEntity<ProductDTO> get(@PathVariable("id") long id) {
		return new ResponseEntity<>(service.get(id), HttpStatus.OK);
	}

	@PostMapping()
	@Override
	public ResponseEntity<ProductDTO> save(@RequestBody @Valid ProductDTO dto) {
		return new ResponseEntity<>(service.save(dto), HttpStatus.CREATED);
	}

	

}
