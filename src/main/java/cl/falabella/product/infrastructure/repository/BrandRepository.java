package cl.falabella.product.infrastructure.repository;

import java.sql.PreparedStatement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import cl.falabella.product.application.port.output.IBrandRepository;
import cl.falabella.product.domain.entity.Brand;

@Repository
public class BrandRepository implements IBrandRepository {
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public BrandRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public Brand get(int id) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT id, name, createdat, updatedat, active ");
		query.append("FROM brand ");
		query.append("WHERE id = ?");
		
		return DataAccessUtils.singleResult(jdbcTemplate.query(query.toString(), new BeanPropertyRowMapper<>(Brand.class), id));
	}

	@Override
	public Brand get(String name) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT id, name, createdat, updatedat, active ");
		query.append("FROM brand ");
		query.append("WHERE UPPER(name) = ?");
		
		return DataAccessUtils.singleResult(jdbcTemplate.query(query.toString(), new BeanPropertyRowMapper<>(Brand.class), name.toUpperCase().trim()));
	}

	@Override
	public Brand save(Brand brand) {
		StringBuilder query = new StringBuilder();
		
		query.append("INSERT INTO brand ( ");
		query.append("name, ");
		query.append("createdat, ");
        query.append("active ) ");
        query.append("VALUES (?, ?, ?)");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query.toString(), new String[]{"id"});
            ps.setObject(1, brand.getName());
            ps.setObject(2, brand.getCreatedAt());
            ps.setObject(3, brand.isActive());
            return ps;
        }, keyHolder);
        
        brand.setId(keyHolder.getKey().intValue());
		
		return brand;
	}
	
}
