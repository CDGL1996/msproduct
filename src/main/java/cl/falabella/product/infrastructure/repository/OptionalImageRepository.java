package cl.falabella.product.infrastructure.repository;

import java.sql.PreparedStatement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import cl.falabella.product.application.port.output.IOptionalImageRepository;
import cl.falabella.product.domain.entity.OptionalImage;

@Repository
public class OptionalImageRepository implements IOptionalImageRepository {
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public OptionalImageRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public OptionalImage get(String productName) {
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT o.id, o.idproduct, o.createdat, o.updatedat, o.active ");
		query.append("FROM optionalimage o ");
		query.append("INNER JOIN product p ON p.id = o.idproduct ");
		query.append("WHERE UPPER(p.name) = ?");
		
		return DataAccessUtils.singleResult(jdbcTemplate.query(query.toString(), new BeanPropertyRowMapper<>(OptionalImage.class), productName.toUpperCase().trim()));
	}

	@Override
	public OptionalImage save(OptionalImage optionalImage) {
		StringBuilder query = new StringBuilder();
		
		query.append("INSERT INTO optionalimage ( ");
		query.append("idproduct, ");
		query.append("createdat, ");
        query.append("active ) ");
        query.append("VALUES (?, ?, ?)");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query.toString(), new String[]{"id"});
            ps.setObject(1, optionalImage.getIdProduct());
            ps.setObject(2, optionalImage.getCreatedAt());
            ps.setObject(3, optionalImage.isActive());
            return ps;
        }, keyHolder);
        
        optionalImage.setId(keyHolder.getKey().longValue());
		
		return optionalImage;
	}

}
