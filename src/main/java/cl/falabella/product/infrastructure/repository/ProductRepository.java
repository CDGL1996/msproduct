package cl.falabella.product.infrastructure.repository;

import java.sql.PreparedStatement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import cl.falabella.product.application.dto.in.ProductDTO;
import cl.falabella.product.application.port.output.IProductRepository;
import cl.falabella.product.domain.entity.Product;

@Repository
public class ProductRepository implements IProductRepository {
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public ProductRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<ProductDTO> get() {
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT p.sku, b.name AS brand, p.name, p.size, p.price, ");
		query.append("p.image, p.createdat, p.updatedat, p.active ");
		query.append("FROM product p ");
		query.append("INNER JOIN brand b ON b.id = p.idbrand");
		
		return jdbcTemplate.query(query.toString(), new BeanPropertyRowMapper<>(ProductDTO.class));
	}
	
	@Override
	public Product get(long id) {
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT id, sku, idbrand, name, size, price, image, createdat, updatedat, active ");
		query.append("FROM product ");
		query.append("WHERE id = ? ");
		
		return DataAccessUtils.singleResult(jdbcTemplate.query(query.toString(), new BeanPropertyRowMapper<>(Product.class), id));
	}
	
	@Override
	public Product get(String name) {
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT sku, idbrand, name, size, price, image, createdat, updatedat, active ");
		query.append("FROM product ");
		query.append("WHERE UPPER(name) = ?");
		
		return DataAccessUtils.singleResult(jdbcTemplate.query(query.toString(), new BeanPropertyRowMapper<>(Product.class), name.toUpperCase().trim()));
	}

	@Override
	public Product save(Product product) {
		StringBuilder query = new StringBuilder();
		
		query.append("INSERT INTO product ( ");
		query.append("sku, ");
		query.append("idbrand, ");
        query.append("name, ");
        query.append("size, ");
        query.append("price, ");
        query.append("image, ");
        query.append("createdat, ");
        query.append("active ) ");
        query.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query.toString(), new String[]{"id"});
            ps.setObject(1, product.getSku());
            ps.setObject(2, product.getIdBrand());
            ps.setObject(3, product.getName());
            ps.setObject(4, product.getSize());
            ps.setObject(5, product.getPrice());
            ps.setObject(6, product.getImage());
            ps.setObject(7, product.getCreatedAt());
            ps.setObject(8, product.isActive());
            return ps;
        }, keyHolder);
        
        product.setId(keyHolder.getKey().longValue());
		
		return product;
	}
	
}