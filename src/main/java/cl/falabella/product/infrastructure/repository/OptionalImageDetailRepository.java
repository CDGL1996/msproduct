package cl.falabella.product.infrastructure.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.falabella.product.application.port.output.IOptionalImageDetailRepository;
import cl.falabella.product.domain.entity.OptionalImageDetail;

@Repository
public class OptionalImageDetailRepository implements IOptionalImageDetailRepository {
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public OptionalImageDetailRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<OptionalImageDetail> get(long idProduct) {
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT d.id, d.idoptionalimage, d.url, d.createdat, d.updatedat, d.active ");
		query.append("FROM optionalimagedetail d ");
		query.append("INNER JOIN optionalimage o ON o.id = d.idoptionalimage ");
		query.append("WHERE o.idproduct = ?");
		
		return jdbcTemplate.query(query.toString(), new BeanPropertyRowMapper<>(OptionalImageDetail.class), idProduct);
	}

	@Override
	public List<OptionalImageDetail> saveAll(long idProduct, List<OptionalImageDetail> imageList) {
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO optionalimagedetail (idoptionalimage, url, createdat, active) VALUES (?, ?, ?, ?)");
		
		this.jdbcTemplate.batchUpdate(
	            query.toString(),
	            new BatchPreparedStatementSetter() {

	                public void setValues(PreparedStatement ps, int i) throws SQLException {
	                    ps.setLong(1, imageList.get(i).getIdOptionalImage());
	                    ps.setString(2, imageList.get(i).getUrl());
	                    ps.setObject(3, imageList.get(i).getCreatedAt());
	                    ps.setBoolean(4, imageList.get(i).isActive());
	                }

	                public int getBatchSize() {
	                    return imageList.size();
	                }

	            });
		
		return imageList;
		
	}

}