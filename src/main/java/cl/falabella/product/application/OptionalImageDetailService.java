package cl.falabella.product.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.falabella.product.application.port.interactor.IOptionalImageDetailService;
import cl.falabella.product.application.port.output.IOptionalImageDetailRepository;
import cl.falabella.product.domain.entity.OptionalImageDetail;

@Transactional
@Service
public class OptionalImageDetailService implements IOptionalImageDetailService {
	
	private final IOptionalImageDetailRepository repository;
	
	@Autowired
	public OptionalImageDetailService(IOptionalImageDetailRepository repository) {
		this.repository = repository;
	}

	@Override
	public List<OptionalImageDetail> get(long idProduct) {
		return repository.get(idProduct);
	}

	@Override
	public List<OptionalImageDetail> saveAll(long idProduct, List<OptionalImageDetail> imageList) {
		List<OptionalImageDetail> existingOptionalImages = get(idProduct);
		
		if(existingOptionalImages.isEmpty()) {
			return repository.saveAll(idProduct, imageList);
		} else {
			return existingOptionalImages;
		}
		
	}

}
