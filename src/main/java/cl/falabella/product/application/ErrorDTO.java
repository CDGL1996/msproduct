package cl.falabella.product.application;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDTO {
	
	private Integer code;
	private String url;
	private String message;
	
}
