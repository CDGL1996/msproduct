package cl.falabella.product.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.falabella.product.application.port.interactor.IOptionalImageService;
import cl.falabella.product.application.port.output.IOptionalImageRepository;
import cl.falabella.product.domain.entity.OptionalImage;

@Transactional
@Service
public class OptionalImageService implements IOptionalImageService {
	
	private final IOptionalImageRepository repository;
	
	@Autowired
	public OptionalImageService(IOptionalImageRepository repository) {
		this.repository = repository;
	}

	@Override
	public OptionalImage get(String productName) {
		return repository.get(productName.toUpperCase().trim());
	}

	@Override
	public OptionalImage save(String productName, OptionalImage optionalImage, List<String> imageList) {
		OptionalImage existingOptionalImage = get(productName.toUpperCase().trim());
		
		if(existingOptionalImage == null) {
			return repository.save(optionalImage);
		} else {
			return existingOptionalImage;
		}
	}

}
