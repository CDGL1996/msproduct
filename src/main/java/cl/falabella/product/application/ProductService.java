package cl.falabella.product.application;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.falabella.product.application.dto.in.ProductDTO;
import cl.falabella.product.application.port.interactor.IBrandService;
import cl.falabella.product.application.port.interactor.IOptionalImageDetailService;
import cl.falabella.product.application.port.interactor.IOptionalImageService;
import cl.falabella.product.application.port.interactor.IProductService;
import cl.falabella.product.application.port.output.IProductRepository;
import cl.falabella.product.domain.entity.Brand;
import cl.falabella.product.domain.entity.OptionalImage;
import cl.falabella.product.domain.entity.OptionalImageDetail;
import cl.falabella.product.domain.entity.Product;

@Transactional
@Service
public class ProductService implements IProductService {
	
	private final IBrandService brandService;
	private final IOptionalImageService optionalImageService;
	private final IOptionalImageDetailService optionalImageDetailService;
	private final IProductRepository repository;
	
	@Autowired
	public ProductService(IBrandService brandService, IOptionalImageService optionalImageService, 
			IOptionalImageDetailService optionalImageDetailService, IProductRepository repository) {
		
		this.brandService = brandService;
		this.optionalImageService = optionalImageService;
		this.optionalImageDetailService = optionalImageDetailService;
		this.repository = repository;
	}

	@Override
	public List<ProductDTO> get() {
		return repository.get();
	}
	
	@Override
	public ProductDTO get(long id) {
		ProductDTO dto = new ProductDTO();
		Product product = repository.get(id);
		
		if(product == null) {
			throw new ApplicationException("No product has been found with this ID!", HttpStatus.NOT_FOUND);
		} else {
			
			Brand brand = brandService.get(product.getIdBrand());
			
			dto.setBrand(brand.getName());
			dto.setName(product.getName());
			dto.setSize(product.getSize());
			dto.setImage(product.getImage());
			dto.setSku(product.getSku());
			dto.setPrice(product.getPrice());
			
			OptionalImage optionalImage = optionalImageService.get(product.getName());
			if(optionalImage != null) {
				List<OptionalImageDetail> imageList = optionalImageDetailService.get(product.getId());
				List<String> imagesDTO = new ArrayList<>();
								
				for(OptionalImageDetail image : imageList) {
					imagesDTO.add(image.getUrl());
				}
				dto.setOptionalImages(imagesDTO);
				
			}
			
		}
		return dto;
	}
	
	@Override
	public Product get(String name) {
		return repository.get(name.toUpperCase().trim());
	}
	
	@Override
	public ProductDTO save(ProductDTO dto) {
		if(get(dto.getName()) == null) {
			
			Brand brand = new Brand();
			brand.setName(dto.getBrand());
			brand.setCreatedAt(LocalDateTime.now());
			brand.setActive(true);
			brand = brandService.save(brand);
			
			Product product = new Product();
			product.setSku(dto.getSku());
			product.setIdBrand(brand.getId());
			product.setName(dto.getName().trim());
			product.setSize(dto.getSize().trim());
			product.setPrice(dto.getPrice());
			product.setImage(dto.getImage().trim());
			product.setCreatedAt(LocalDateTime.now());
			product.setActive(true);
			product = repository.save(product);
			
			if(!dto.getOptionalImages().isEmpty()) {
				
				OptionalImage optionalImage = new OptionalImage();
				optionalImage.setIdProduct(product.getId());
				optionalImage.setCreatedAt(LocalDateTime.now());
				optionalImage.setActive(true);
				optionalImage = optionalImageService.save(product.getName(), optionalImage, dto.getOptionalImages());
				
				List<OptionalImageDetail> imageList = new ArrayList<>();
				
				for(String image : dto.getOptionalImages()) {
					OptionalImageDetail optionalImageCreated = new OptionalImageDetail();
					optionalImageCreated.setIdOptionalImage(optionalImage.getId());
					optionalImageCreated.setUrl(image);
					optionalImageCreated.setCreatedAt(LocalDateTime.now());
					optionalImageCreated.setActive(true);
					imageList.add(optionalImageCreated);
				}
				
				optionalImageDetailService.saveAll(product.getId(), imageList);
			}
			return dto;
			
		} else {
			throw new ApplicationException("There's already a product with this name!", HttpStatus.BAD_REQUEST);
		}
		
	}

}