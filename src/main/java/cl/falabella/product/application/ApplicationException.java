package cl.falabella.product.application;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class ApplicationException extends RuntimeException implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private final HttpStatus httpStatus;

	public ApplicationException(String errorMessage, HttpStatus httpStatus){
        super(errorMessage);
        this.httpStatus = httpStatus;
    }

}
