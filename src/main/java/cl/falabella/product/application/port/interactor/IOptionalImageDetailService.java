package cl.falabella.product.application.port.interactor;

import java.util.List;

import cl.falabella.product.domain.entity.OptionalImageDetail;

public interface IOptionalImageDetailService {
	
	List<OptionalImageDetail> get(long idProduct);
	List<OptionalImageDetail> saveAll(long idProduct, List<OptionalImageDetail> imageList);

}
