package cl.falabella.product.application.port.interactor;

import java.util.List;

import cl.falabella.product.application.dto.in.ProductDTO;
import cl.falabella.product.domain.entity.Product;

public interface IProductService {
	
	List<ProductDTO> get();
	ProductDTO get(long id);
	Product get(String name);
	ProductDTO save(ProductDTO dto);

}
