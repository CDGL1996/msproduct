package cl.falabella.product.application.port.interactor;

import java.util.List;

import cl.falabella.product.domain.entity.OptionalImage;

public interface IOptionalImageService {
	
	OptionalImage get(String productName);
	OptionalImage save(String productName, OptionalImage optionalImage, List<String> imageList);

}
