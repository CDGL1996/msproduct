package cl.falabella.product.application.port.output;

import cl.falabella.product.domain.entity.Brand;

public interface IBrandRepository {
	
	Brand get(int id);
	Brand get(String name);
	Brand save(Brand brand);

}
