package cl.falabella.product.application.port.output;

import java.util.List;

import cl.falabella.product.domain.entity.OptionalImageDetail;

public interface IOptionalImageDetailRepository {
	
	List<OptionalImageDetail> get(long idProduct);
	List<OptionalImageDetail> saveAll(long idProduct, List<OptionalImageDetail> imageList);

}
