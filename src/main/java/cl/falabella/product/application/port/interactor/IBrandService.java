package cl.falabella.product.application.port.interactor;

import cl.falabella.product.domain.entity.Brand;

public interface IBrandService {
	
	Brand get(int id);
	Brand get(String name);
	Brand save(Brand brand);

}
