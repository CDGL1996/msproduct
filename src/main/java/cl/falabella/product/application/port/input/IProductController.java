package cl.falabella.product.application.port.input;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import cl.falabella.product.application.dto.in.ProductDTO;

public interface IProductController {
	
	ResponseEntity<List<ProductDTO>> get();
	ResponseEntity<ProductDTO> get(@PathVariable("id") long id);
	ResponseEntity<ProductDTO> save(@RequestBody @Valid ProductDTO dto);

}
