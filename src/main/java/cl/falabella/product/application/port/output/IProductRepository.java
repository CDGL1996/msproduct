package cl.falabella.product.application.port.output;

import java.util.List;

import cl.falabella.product.application.dto.in.ProductDTO;
import cl.falabella.product.domain.entity.Product;

public interface IProductRepository {
	
	List<ProductDTO> get();
	Product get(long id);
	Product get(String name);
	Product save(Product product);

}
