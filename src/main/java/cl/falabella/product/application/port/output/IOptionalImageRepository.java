package cl.falabella.product.application.port.output;

import cl.falabella.product.domain.entity.OptionalImage;

public interface IOptionalImageRepository {
	
	OptionalImage get(String productName);
	OptionalImage save(OptionalImage optionalImage);

}
