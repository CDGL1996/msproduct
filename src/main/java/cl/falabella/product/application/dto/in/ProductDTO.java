package cl.falabella.product.application.dto.in;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDTO {
	
	@NotNull(message = "The product's SKU cannot be null.")
	@Min(1000000)
	@Max(99999999)
	private Long sku;
	
	@NotNull(message = "The product's brand cannot be null.")
	@NotBlank(message = "The product's brand cannot be blank.")
	@Size(min = 3, max = 50, message = "The product's brand must contain between 3 and 50 characters.")
	private String brand;
	
	@NotNull(message = "The product's name cannot be null")
	@NotBlank(message = "The product's name cannot be blank")
	@Size(min = 3, max = 50, message = "The product's name must contain between 3 and 50 characters.")
	private String name;
	
	@NotBlank(message = "The product's size cannot be blank.")
	private String size;
	
	@NotNull(message = "The product's price cannot be null.")
	@Min(value = 0, message = "The product's price cannot be under 0.")
	private Long price;
	
	@NotNull(message = "The product's image is mandatory.")
	private String image;
	
	private List<String> optionalImages;

}
