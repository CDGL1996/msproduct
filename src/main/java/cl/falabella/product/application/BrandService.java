package cl.falabella.product.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.falabella.product.application.port.interactor.IBrandService;
import cl.falabella.product.application.port.output.IBrandRepository;
import cl.falabella.product.domain.entity.Brand;

@Transactional
@Service
public class BrandService implements IBrandService {
	
	private final IBrandRepository repository;
	
	@Autowired
	public BrandService(IBrandRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public Brand get(int id) {
		return repository.get(id);
	}

	@Override
	public Brand get(String name) {
		return repository.get(name.toUpperCase().trim());
	}

	@Override
	public Brand save(Brand brand) {
		Brand existingBrand = get(brand.getName());
		
		if(existingBrand == null) {
			return repository.save(brand);
		} else {
			return existingBrand;
		}
	}
}
